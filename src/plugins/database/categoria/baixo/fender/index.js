export default {
  nome: [
    'Baixo Fender',
    'Baixo Fender'
  ],
  descricao: [
    'Original American Bass',
    'Precision Stratocaster'
  ],
  preco: [
    '3.500',
    '2.300'
  ],
  imagem: [
    require('@/assets/img/categoria/baixo-fender-original-american-bass.png'),
    require('@/assets/img/categoria/baixo-fender-precision-stratocaster.png')
  ],
  especificacao: [
    {
      braco: [
        'Braço',
        'Asher'
      ],
      corpo: [
        'Corpo',
        'Maple'
      ],
      ferragens: [
        'Ferragens',
        'Gotho'
      ],
      captadorBraco: [
        'Captador Braço',
        'Bartolini'
      ],
      captadorPonte: [
        'Captador Ponte',
        'EMG'
      ]
    },
    {
      braco: [
        'Braço',
        'Asher'
      ],
      corpo: [
        'Corpo',
        'Rosewood'
      ],
      ferragens: [
        'Ferragens',
        'Gotho'
      ],
      captadorBraco: [
        'Captador Braço',
        'Fender'
      ],
      captadorPonte: [
        'Captador Ponte',
        'EMG'
      ]
    }
  ]
}
