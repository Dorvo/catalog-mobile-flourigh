export default {
  title: 'Som Mexe Music - Catalog',

  toggle: {
    drawer: false,
    search: false
  },

  footer: {
    icon: [
      'fab fa-youtube',
      'fab fa-instagram',
      'fab fa-twitter',
      'fab fa-facebook',
      'fab fa-whatsapp',
      'fab fa-google',
      'fas fa-map-marked-alt'
    ],

    link: [
      'https://www.youtube.com/user/sommexe',
      'https://www.instagram.com/Renatosommexe',
      'https://www.twitter.com/sommexe',
      'https://www.facebook.com/sommexe',
      'https://api.whatsapp.com/send?phone=5521964661433',
      'https://www.google.com/search?q=Som+Mexe',
      'https://www.goo.gl/maps/vruB8tJ97RJRxUxU7'
    ]
  }
}
